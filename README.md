# Sat

## Summary

Sat is a process sample showing how distributed in memory applications may work in the data streaming decade. The design was inspired by Docker and the Windows/Linux page memory management system.

It is a simple process that stores a browser document that can be shared with the url of the hashed content.

## Getting started

Let's just launch with a configuration file and an empty process.

```
IMPLEMENTATION=https://data.schmied.us/e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855.tig go run main.go
```

Launch the browser

```
firefox http://127.0.0.1:7777/sat
```

## Configuration

Use https://gitlab.com/eper.io/tig for the data backend to save snapshots and share. You can run your own tig server.

```
Save memory snapshot every 2 minutes.
Set backend to https://tig.example.com?apikey=abcde value.
Load memory snapshot from https://data.schmied.us/e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855.tig value.
```

Launch the app with these settings

```
IMPLEMENTATION=https://tig.example.com/f26...65cc.tig go run main.go
```

Launch the browser

```
firefox http://127.0.0.1:7777/sat
```

## References

### Modern Columnar Databases

ISBN-13: 9783319373898

ISBN-13: 978-0131873254

