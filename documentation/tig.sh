#!/bin/bash

# Prerequisite
# export DATA=https://example.com?apikey=abcd
# export IMPLEMENTATION=https://example.com/abcd.tig

tar --exclude './.git' -c . | curl -X PUT --data-binary @- $DATA'&format=https://data.schmied.us%25s' | xargs -n 1 -I {} printf "docker stop --time 2 sat8525;sleep 3;docker run --name sat8525 -d --rm -e IMPLEMENTATION=$IMPLEMENTATION -e CODE={} -p 8525:7777 golang:1.19.3 bash -c 'curl \$CODE | tar -x -C /go/src/;cd /go/src/;go run main.go'\n" | curl -X PUT --data-binary @- $DATA'&format=https://data.schmied.us%25s'

