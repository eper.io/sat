package metadata

import (
	"bufio"
	"bytes"
	"fmt"
	"io"
	"net/http"
	"net/url"
	"os"
	path2 "path"
	"strings"
	"time"
)

// This work is marked with CC0 1.0
// https://creativecommons.org/publicdomain/zero/1.0/legalcode.txt

// export DATA=<https://tig.example.com?apikey=example>
var MemCacheDaemon = "https://tmp.schmied.us"
var ShortBlockCache = map[string][]byte{}
var BlockList = make([]string, 0)
var LastSnapshot = ""

// This should only get access from this process or Docker container
// It is ideally the $(HOME)
var Dir = "/tmp"

func Setup() {
	// We use a very conservative approach to logging.
	// We log the configuration input, but nothing else.
	// Extensive logging just opens vulnerabilities.
	// It also costs a lot with variable returns.
	// We rather make sure that the run can be reproduced.
	implementation := os.Getenv("IMPLEMENTATION")
	if implementation != "" {
		resp, _ := http.Get(implementation)
		if resp != nil && resp.Body != nil {
			lines := bufio.NewScanner(resp.Body)
			for lines.Scan() {
				line := strings.TrimSpace(lines.Text())
				var value string
				n, _ := fmt.Sscanf(line, "Set apikey to %s value.", &value)
				if n == 1 {
					_ = os.WriteFile(path2.Join(Dir, "apikey"), Fetch(value), 0600)
					fmt.Println(line)
				}
				n, _ = fmt.Sscanf(line, "Set certificate.pem to %s value.", &value)
				if n == 1 {
					_ = os.WriteFile(path2.Join(Dir, "certificate.pem"), Fetch(value), 0600)
					fmt.Println(line)
				}
				n, _ = fmt.Sscanf(line, "Set key.pem to %s value.", &value)
				if n == 1 {
					_ = os.WriteFile(path2.Join(Dir, "key.pem"), Fetch(value), 0600)
					fmt.Println(line)
				}
				n, _ = fmt.Sscanf(line, "Set backend to %s value.", &value)
				if n == 1 {
					MemCacheDaemon = value
					fmt.Println(line)
				}
				n, _ = fmt.Sscanf(line, "Load memory snapshot from %s value.", &value)
				if n == 1 {
					fmt.Println(line)
					LastSnapshot = value
					snapshotBytes := Fetch(value)
					snapshotReader := bytes.NewBuffer(snapshotBytes)
					snapshotLines := bufio.NewScanner(snapshotReader)
					for snapshotLines.Scan() {
						lineNumber := 0
						value = snapshotLines.Text()
						n, _ = fmt.Sscanf(value, "Block number %06d is hashed as %s value.", &lineNumber, &value)
						if n > 1 {
							value = value[len(value)-64-len("/.tig") : len(value)]
							BlockList = append(BlockList, value)
							target, err := url.Parse(MemCacheDaemon)
							if err != nil {
								return
							}
							target.Path = fmt.Sprintf("%s", value)
							target.RawQuery = ""
							data := Fetch(target.String())
							fmt.Println(string(data))
							if data != nil && len(data) > 0 && len(data) < 512 {
								ShortBlockCache[value] = data
							}
						}
					}
				}
				minutes := int64(0)
				n, _ = fmt.Sscanf(line, "Save memory snapshot every %d minutes.", &minutes)
				if n == 1 {
					fmt.Println(line)
					go func() {
						i := 0
						start := time.Now()
						o := time.Duration(0)
						for {
							time.Sleep(time.Duration(minutes) * time.Minute)
							buf := bytes.NewBuffer([]byte{})
							for k, v := range BlockList {
								buf.WriteString(fmt.Sprintf("Block number %06d is hashed as %s value.", k, v) + "\n")
							}
							buf1 := Poke(MemCacheDaemon+"&format=%25s", buf.Bytes())
							root, _, _ := strings.Cut(MemCacheDaemon, "#")
							LastSnapshot = root + string(buf1)
							fmt.Printf(".")
							i++
							if i%10 == 0 {
								fmt.Printf("\n")
							}
							m := time.Now().Sub(start) / 24 * time.Hour
							if m > o+1 {
								o = m
								if len(LastSnapshot) > 5 {
									fmt.Printf("%s... set as snapshot.\n", LastSnapshot[0:5])
								}
							}
						}
					}()
				}
			}
			_ = resp.Body.Close()
		}
	}
}

func Fetch(url string) (ret []byte) {
	resp, _ := http.Get(url)
	if resp != nil && resp.Body != nil {
		ret, _ = io.ReadAll(resp.Body)
	}
	return
}

func Poke(url string, input []byte) (ret []byte) {
	ret = []byte{}
	client := http.Client{
		Timeout: 30 * time.Second,
	}

	req, err := http.NewRequest(http.MethodPost, url, bytes.NewBuffer(input))
	if err != nil {
		// Do you need an error? Check the server side.
		// Logs are an attack surface.
		return
	}
	req.Header.Set("Content-Type", "application/json")
	res, err := client.Do(req)
	if err != nil {
		// Do you need an error? Check the server side.
		// Logs are an attack surface.
		return
	}
	ret, _ = io.ReadAll(res.Body)
	_ = res.Body.Close()
	return
}
