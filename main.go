package main

import (
	"bytes"
	"crypto/sha256"
	"crypto/tls"
	"fmt"
	"io"
	"log"
	"net/http"
	"net/url"
	"os"
	"path"
	"sat.eper.io/metadata"
	"strings"
)

// This work is marked with CC0 1.0
// https://creativecommons.org/publicdomain/zero/1.0/legalcode.txt

func main() {
	metadata.Setup()
	Setup()

	_, err1 := tls.LoadX509KeyPair(path.Join(metadata.Dir, "certificate.pem"), path.Join(metadata.Dir, "key.pem"))
	if err1 != nil {
		fmt.Println(err1)
	}
	_, err := os.Stat(path.Join(metadata.Dir, "key.pem"))
	if err == nil {
		err = http.ListenAndServeTLS(":7777", path.Join(metadata.Dir, "certificate.pem"), path.Join(metadata.Dir, "key.pem"), nil)
	} else {
		err = http.ListenAndServe(":7777", nil)
	}
	if err != nil {
		log.Fatal("ListenAndServe: ", err)
	}
}

func Setup() {
	const prefix = "/sat"
	http.HandleFunc(prefix+"", func(writer http.ResponseWriter, request *http.Request) {
		writer.Header().Set("Cache-Control", "no-store")
		buf, _ := os.ReadFile("./res/sat.html")
		h := string(buf)
		h = strings.ReplaceAll(h, "/res/", prefix+"/res/")
		h = strings.ReplaceAll(h, "https://example.com/", prefix+"/data/")
		h = strings.ReplaceAll(h, "'/data'", "'"+prefix+"/data'")
		h = strings.ReplaceAll(h, "src=\"/data", "src=\""+prefix+"/data")
		_, _ = io.WriteString(writer, h)
	})
	http.HandleFunc(prefix+"/res/", func(writer http.ResponseWriter, request *http.Request) {
		writer.Header().Set("Cache-Control", "no-store")
		Dynamic(writer, request, prefix)
	})

	const data = prefix + "/data/"
	http.HandleFunc(data, func(writer http.ResponseWriter, request *http.Request) {
		if request.URL.Path[len(data):] == "englang/index" {
			var contentType = "text/plain"
			if request.URL.Query().Has("Content-Type") {
				contentType = request.URL.Query().Get("Content-Type")
			}

			writer.Header().Set("Content-Type", contentType)
			var snapshot = metadata.BlockList
			for k, v := range snapshot {
				if contentType == "text/plain" {
					_, _ = io.WriteString(writer, v+"\n")
				}
				if contentType == "text/html" {
					_, _ = io.WriteString(writer, fmt.Sprintf("<a href=\"%s\">Englang log #%06d</a>.<br>\n", v, k)+"\n")
				}
			}
		}
		if request.URL.Path[len(data):] == "englang" {
			var contentType = "text/plain"
			if request.URL.Query().Has("Content-Type") {
				contentType = request.URL.Query().Get("Content-Type")
			}

			target, err := url.Parse(metadata.MemCacheDaemon)
			if err != nil {
				return
			}

			var snapshot = metadata.BlockList

			MapSnapshot(writer, request, snapshot, target, data, func(path string, content io.Reader) {
				proxyPath := fmt.Sprintf("/%s?Content-Type=text/plain", path[len(data):])
				if contentType == "text/plain" {
					_, _ = io.WriteString(writer, "\n"+proxyPath+"\n")
				}
				if contentType == "text/plain" {
					_, _ = io.Copy(writer, content)
				}
			})
		}

		target, err := url.Parse(metadata.MemCacheDaemon)
		if err != nil {
			return
		}

		target.Path = request.URL.Path[len(data):]
		q := url.Values{}
		for k, v := range target.Query() {
			if len(v) > 0 {
				q.Add(k, v[0])
			}
		}
		for k, v := range request.URL.Query() {
			if len(v) > 0 {
				q.Add(k, v[0])
			}
		}
		target.RawQuery = q.Encode()

		client := &http.Client{}
		snapshot := bytes.Buffer{}
		_, _ = io.Copy(&snapshot, request.Body)
		buf := snapshot.Bytes()
		if request.Method == "PUT" {
			stored := data + fmt.Sprintf("%x.tig", sha256.Sum256(buf))
			if len(buf) <= 512 {
				metadata.ShortBlockCache[stored] = buf
			}
			metadata.BlockList = append(metadata.BlockList, stored)
		}
		upload := bytes.NewBuffer(buf)
		req, err := http.NewRequest(request.Method, target.String(), upload)
		if err != nil {
			writer.WriteHeader(http.StatusInternalServerError)
			_, _ = io.WriteString(writer, "request error")
			return
		}

		resp, err := client.Do(req)
		if err != nil {
			writer.WriteHeader(http.StatusInternalServerError)
			_, _ = io.WriteString(writer, "proxy error")
			return
		}

		if resp.Body != nil {
			_, _ = io.Copy(writer, resp.Body)
		}
		_ = resp.Body.Close()
	})
}

func MapSnapshot(writer http.ResponseWriter, request *http.Request, snapshot []string, target *url.URL, prefix string, core func(path string, content io.Reader)) {
	for _, v := range snapshot {
		proxyHost := fmt.Sprintf("%s://%s", target.Scheme, target.Host)
		proxyPath := fmt.Sprintf("/%s?Content-Type=text/plain", v[len(prefix):])
		client := &http.Client{}
		req, err := http.NewRequest(request.Method, proxyHost+proxyPath, nil)
		if err != nil {
			_, _ = io.WriteString(writer, target.String())
			continue
		}

		resp, err := client.Do(req)
		if err != nil {
			_, _ = io.WriteString(writer, target.String())
			continue
		}

		if resp.Body != nil {
			core(v, resp.Body)
		}
		_ = resp.Body.Close()
	}
}

func Dynamic(writer http.ResponseWriter, request *http.Request, prefix string) {
	if !strings.HasPrefix(request.URL.Path, prefix+"/res/") {
		writer.WriteHeader(http.StatusUnauthorized)
		return
	}
	name := path.Join("./", request.URL.Path[len(prefix):])
	f, _ := os.Stat(name)
	if f == nil {
		writer.WriteHeader(http.StatusNotFound)
		return
	}
	enc := map[string]string{"html": "text/html", "png": "image/png", "jpg": "image/jpeg", "jpeg": "image/jpeg", "gif": "image/gif", "css": "text/css", "js": "text/javascript"}
	for k, v := range enc {
		if strings.HasSuffix(f.Name(), k) {
			writer.Header().Set("Content-Type", v)
		}
	}
	h, _ := os.ReadFile(name)
	_, _ = io.WriteString(writer, strings.ReplaceAll(string(h), "/res/bootstrap-5.3.2-dist", prefix+"/res/bootstrap-5.3.2-dist"))
}

func GetFields(s string, f ...string) (y []string) {
	x := append(make([]string, 0), f...)
	y = make([]string, 0)
	for k, v := range x {
		b, e, q := strings.Cut(s, v)
		if !q {
			return
		}
		if k > 0 {
			y = append(y, b)
		}
		s = e
	}
	return
}
